import pygame
import random


# Khởi tạo
pygame.init()
clock =pygame.time.Clock()
fps =30
# screen
screenWidth = 600
screenHeight = 700
screen = pygame.display.set_mode([screenWidth, screenHeight])
pygame.display.set_caption("Bullet")

#img backkground
background = pygame.image.load('./img/background.png')

#variables in game
backgroundScroll = 0
scrollSpeed = 4
dragonCreate = 3000
lastDragon = pygame.time.get_ticks()
red= (255, 0, 0)
green = (0, 255, 0)


#Egle class
class Egle(pygame.sprite.Sprite):
    def __init__(self, x,y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []
        self.index = 0 
        self.counter = 0
        for i in range(1,5):
            img = pygame.image.load(f'./img/fly{i}.png')
            self.images.append(img)
        self.image = self.images[self.index]
        self.rect = self.image.get_rect()
        self.rect.center = [x,y]
        self.lastShoot = pygame.time.get_ticks()
    def update(self):
        #key press for control
        speed = 8
        key = pygame.key.get_pressed()
        if key[pygame.K_UP] and self.rect.top > 0 :
            self.rect.y -= speed
        if key[pygame.K_DOWN] and self.rect.bottom < screenHeight:
            self.rect.y += speed
        #shooting
        timeNow = pygame.time.get_ticks()
        cooldownShoot = 500
        if timeNow - self.lastShoot > cooldownShoot:
            bullets = Bullets(self.rect.centerx-60,self.rect.centery-66)
            bulletsGroup.add(bullets)
            self.lastShoot = timeNow
        #animation for character
        self.counter += 1
        cooldown = 3
        if self.counter > cooldown:
            self.counter = 0
            self.index += 1
            if self.index >= len(self.images):
                self.index = 0
        self.image = self.images[self.index]  
        self.image = pygame.transform.scale(self.image, (80, 100))

#Bullets class
class Bullets(pygame.sprite.Sprite):
    def __init__(self, x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('./img/bullets1.png')
        self.rect = self.image.get_rect()
        # self.image = pygame.transform.scale(self.image, (30, 10))
        self.rect.center = [x,y]
    
    def update(self):
        self.rect.x += 5
        if self.rect.right > 700:
            self.kill()

#Dragon class
class Dragon(pygame.sprite.Sprite):
    def __init__(self, x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('./img/dragon1.png')
        self.rect = self.image.get_rect()
        self.rect.center = [x,y]
        self.countShootToDragon = random.randrange(1,4)
        self.font = pygame.font.SysFont(None, 30)
    def update(self):
        self.text = self.font.render(f'{self.countShootToDragon}', True, red, green)
        screen.blit(self.text,(self.rect.x+20,self.rect.y-10))
        self.rect.x -= 2
        if self.rect.left < 0:
            self.kill()
        if pygame.sprite.spritecollide(self,bulletsGroup,True):
            self.countShootToDragon -= 1
        if self.countShootToDragon == 0:
                self.kill()   

#Group egle
egleGroup = pygame.sprite.Group()
egle = Egle(180, int(screenHeight/2))
egleGroup.add(egle)

#Group bullets
bulletsGroup = pygame.sprite.Group()

#Group dragon
dragonGroup = pygame.sprite.Group()



running = True
while running:
    clock.tick(fps)
    screen.blit(background,(backgroundScroll,-320))

    #egle in game
    egleGroup.draw(screen)
    egle.update()

    #bullets in game
    bulletsGroup.draw(screen)
    bulletsGroup.update()

    #dragon in game
    dragonGroup.draw(screen)
    dragonGroup.update()

    timeNow1 = pygame.time.get_ticks()
    if timeNow1 - lastDragon > dragonCreate:
        dragon = Dragon(600,random.randrange(100,500))
        dragonGroup.add(dragon)
        lastDragon = timeNow1
    
    
    backgroundScroll -= scrollSpeed
    if abs(backgroundScroll) > 1100:
        backgroundScroll = 0

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    pygame.display.update()            

pygame.quit()
